use ferris_says::say; // First dependency
use std::io::{stdout, BufWriter}; // Use both stdout and BufWriter

fn main()
{
    println!("Hello, world!"); // Rust print line

    let stdout = stdout(); // Rust uses 'let'
    let out = b"Hello fellow Rustaceans!"; // 'b' signals a byte string
    let width = 24;

    let mut writer = BufWriter::new(stdout.lock()); // 'mut' allows variables to mutate
    say(out, width, &mut writer).unwrap();
}
